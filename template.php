<?php

/**
  * Implements hook_preprocess_field_slideshow()
  * - Used to rip of the extra inner div that field module gives
  * - Initial code snippet is copied from the field Slideshow module file
  */
function fila_server_preprocess_field_slideshow(&$variables) {
  $slide_theme = (isset($variables['breakpoints']) && isset($variables['breakpoints']['mapping']) && !empty($variables['breakpoints']['mapping'])) ? 'picture' : 'image_style';
  foreach ($variables['items'] as $num => $item) {
    // Generate the image render elements
    $image = array();
    $image['path'] = $item['uri'];
    $image['attributes']['class'] = array('field-slideshow-image', 'field-slideshow-image-' . (1+$num));
    $image['alt'] = isset($item['alt']) ? $item['alt'] : '';
    if (isset($item['width']) && isset($item['height'])) {
      $image['width'] = $item['width'];
      $image['height'] = $item['height'];
    }
    elseif ($item["type"] == 'image') {
      $image_dims = getimagesize($image['path']);
      $image['width'] = $image_dims[0];
      $image['height'] = $image_dims[1];
    }
    else {
      $image = array(
        'width' => 0,
        'height' => 0
      );
    }
    if (isset($item['title']) && drupal_strlen($item['title']) > 0) $image['title'] = $item['title'];
    // Code sniipet to render the inner markup of the field Slideshow
    if (isset($variables['image_style']) && $variables['image_style'] != '') {
      $image['style_name'] = $variables['image_style'];
      $image['breakpoints'] = $variables['breakpoints'];
      $variables['items'][$num]['image'] = theme($slide_theme, $image); // Only rendering the image (Extra inner Div ripped off)
    }
    elseif (isset($variables['file_style']) && $variables['file_style'] != '') {
      $media = file_view((object) $variables['items'][$num], $variables['file_style']);
      $variables['items'][$num]['image'] = render($media); // Only rendering the image (Extra inner Div ripped off)
    }
    else {
      $variables['items'][$num]['image'] = theme('image', $image); // Only rendering the image (Extra inner Div ripped off)
    }
  }
}


/**
  * Implementation of hook_preproccess_page()
  */

function fila_server_preprocess_page(&$vars){
  global $user;
  //hiding tabs for logged out user pages
  preg_match('/^user/', current_path(), $matches);
  if(count($matches) > 0 && $user->uid === 0){
    unset($vars['tabs']);
  }
  if(isset($vars['node'])){
    if($vars['node']->type === 'homepage_fila'){
      $vars['theme_hook_suggestions'][] = 'page__node__homepage';
      //Removing the extra add button
      unset($vars['page']['content']['system_main']['nodes'][$vars['node']->vid]['field_fc_sec1_slides']['#prefix']);
      unset($vars['page']['content']['system_main']['nodes'][$vars['node']->vid]['field_fc_sec1_slides']['#suffix']);
    }
  }
  drupal_add_js(array('themePath' => drupal_get_path('theme','fila_server')), array('type'=>'setting'));
}


/**
  *Implementation of hook_preproccess_node()
  */

function fila_server_preprocess_node(&$vars){
  //Adding classes for section 1 to node variables
  if($vars['field_sec1_fg_theme'][0]['value'] === 'blue_theme'){
    $vars['theme_class'] = 'blue-theme';
  } else if($vars['field_sec1_fg_theme'][0]['value'] === 'white_theme'){
    $vars['theme_class'] = 'white-theme';
  }
  // $vars['has_arrow'] = $vars['field_sec1_fg_navigation_control'][0]['value'];
  // Integrating  nivoslider from drupal.settings
  if($vars['type'] === 'homepage_fila' && !$vars['is_front']){
    drupal_add_js(array(
      'slider1Settings' => array(
        'speed'      => (count($vars['field_sec1_fg_animation_speed']) !== 0 ? $vars['field_sec1_fg_animation_speed'][0]['value']: '2000'),
        'pauseTime'      => (count($vars['field_sec1_fg_pause_time']) !== 0? $vars['field_sec1_fg_pause_time'][0]['value']: '1000'),
        'effect'       => 'random',
        'slices' => 10,
        'startSlide' => 0
        )
      )
      ,array('type' => 'setting')
    );

  }
  drupal_add_js(array(
    'nodeType' => $vars['type']
  ),
  array('type' => 'setting'));
}


/**
  *Implements hook_form_alter for login form
  */

function fila_server_form_user_login_alter(&$form , &$form_state){
  // Adding the placeholder
  $form['name']['#attributes'] = array(
    'placeholder' => 'Username'
    );
  $form['pass']['#attributes'] = array(
    'placeholder' => 'Password'
    );
  //Removing the description
  unset($form['pass']['#description']);
  unset($form['name']['#description']);
  $form['forgot_password'] = array(
      '#theme' => 'link',
      '#text' => 'Forgot Password ?',
      '#path' => 'user/password',
      '#options' => array(
        'attributes' => array('class' => array('forgot-password-link'),),
        'html' => TRUE,
      ),
    );

  return $form;
}


/**
  * Implementation hook_form_alter for Add user Form
  */

function fila_server_form_user_register_form_alter(&$form , &$form_state){
  // Adding the legacy Header
  unset($form['account']['status']);
  $form['header'] = array(
    '#type' => 'markup',
    '#markup' => '<h3 class="section_header">Add New User</h3>',
    '#weight' => -101,
    );
  //Altering the order of the forms
  $form['account']['field_role'] = $form['field_role'];
  $form['account']['field_country_homepage'] = $form['field_country_homepage'];
  $form['account']['field_country_homepage']['#weight'] = -80;
  $form['account']['field_role']['#weight'] = -100;
  //Making country dropdown dependent on user role
  $form['account']['field_country_homepage']['#states'] = array(
      'visible'      => array(
        ':input[name="field_role[und]"]' => array('value' => 'country_specific'),
      ),
    );
  // Unsetting the remaining variables
  unset($form['account']['roles']);
  unset($form['field_country_homepage']);
  unset($form['field_role']);

  return $form;
}

/**
  * Implementing theme_field_collection_view()
  * For Scrapping the markup wrapper
  */

function fila_server_field_collection_view(&$vars){
  $element = $vars['element'];
  unset($vars['element']['links']);
  return $element['#children'];
}

/**
  * Implementation hook_form_alter for Edit user Form
  */
function fila_server_form_user_profile_form_alter(&$form , &$form_state){
  // Adding the legacy Header
  unset($form['account']['status']);
  $form['header'] = array(
    '#type' => 'markup',
    '#markup' => '<h3 class="section_header">Edit User - '. $form['#user']->name.'</h3>',
    '#weight' => -101,
    );
  //Altering the order of the forms
  $form['account']['field_role'] = $form['field_role'];
  $form['account']['field_country_homepage'] = $form['field_country_homepage'];
  $form['account']['field_country_homepage']['#weight'] = -80;
  $form['account']['field_role']['#weight'] = -100;
  //Making country dropdown dependent on user role
  $form['account']['field_country_homepage']['#states'] = array(
      'visible'      => array(
        ':input[name="field_role[und]"]' => array('value' => 'country_specific'),
      ),
    );
  // // Unsetting the remaining variables
  unset($form['field_role']);
  unset($form['field_country_homepage']);
  unset($form['account']['roles']);
  unset($form['timezone']);
  unset($form['ckeditor']);
  return $form;
}


function fila_server_css_alter(&$csss){
  $args = arg();
  if($args[0] === 'node' && node_load($args[1])->type === 'homepage_fila'){
    foreach($csss as $key => $css){
      if(strrpos($key, "ember") || strrpos($key, "system")){
        unset($csss[$key]);
      }
    }
  }
}


function fila_server_theme($existing, $type, $theme, $path) {
  $section_theme_path = drupal_get_path( 'theme', 'fila_server' ) . '/templates/sections';
  // print_r($section_theme_path);
  // exit;
  return array(
    'section1_template' => array(
      'template' => 'section1-template',
      'variables' => array(
        'pageVariable' => NULL,
        'st' => '1'
      ),
      'path' =>$section_theme_path
    ),
    'section2_template' => array(
      'template' => 'section2-template',
      'variables' => array(
        'pageVariable' => NULL,
        'st' => '1'
      ),
      'path' =>$section_theme_path
    ),
    'section3_template' => array(
      'template' => 'section3-template',
      'variables' => array(
        'pageVariable' => NULL,
        'st' => '1'
      ),
      'path' =>$section_theme_path
    ),
    'section4_template' => array(
      'template' => 'section4-template',
      'variables' => array(
        'pageVariable' => NULL,
        'st' => '1'
      ),
      'path' =>$section_theme_path
    ),
  );
}
