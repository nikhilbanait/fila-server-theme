(function($) {
  Drupal.behaviors.homePageSettings = {
    attach: function(byCORS) {
      byCORS = byCORS || false;
      // CASE OF CORS
      if (byCORS === true) {
        $('script').each(function() {
          if ($(this).html() !== '' && $(this).html().indexOf('jQuery.extend(Drupal.settings') !== -1) {
            var ini = $(this).html().indexOf('jQuery.extend(Drupal.settings, ') + 'jQuery.extend(Drupal.settings, '.length;
            var fin = $(this).html().indexOf(');');
            var string = $(this).html().substring(ini, fin);
            jQuery.extend(Drupal.settings, JSON.parse(string));
            $('.mainContentRegion').css({
              'padding-top': $('#header').height() + 'px'
            }); // Setting up the height
          }
        });
      }

      $('#block-system-user-menu .content > .menu .expanded').children().wrapAll('<div class="extra-wrapper"></div>');

      $('#block-system-user-menu').find('.content').find('.expanded').find('.extra-wrapper').children('a').click(function(event) {
        event.preventDefault();
        if ($('#block-system-user-menu').find('.content').find('.expanded').hasClass('opened')) {
          $('#block-system-user-menu').find('.content').find('.expanded').removeClass('opened');
          $('#block-system-user-menu').find('.content').find('.expanded').css({
            'width': '77px'
          });
        } else {
          $('#block-system-user-menu').find('.content').find('.expanded').addClass('opened');
          $('#block-system-user-menu').find('.content').find('.expanded').css({
            'width': '250px'
          });
        }
      });

      var imgLoad = imagesLoaded($('.mainContentRegion'));
      ytPLayer = {};

      function onReady(event) {
        var key = event.target.f.id;
        if (event) {
          if (!ytPLayer[key]) {
            ytPLayer[key] = event.target;
          }
          ytPLayer[key].playVideo();
        }
      }

      $('.play-pause-btn').on('click', function(event) {
        var player_id;
        if ($(this).parent().find('.mediaType').attr('data-src') === 'custom') {
          player_id = $(this).parent().find('video').attr('id');
          if ($(this).parent().hasClass('playing')) {
            document.getElementById(player_id).pause();
            $(this).parent().removeClass('playing');
            $(this).parent().find('.overlay-image').fadeIn(200);
          } else {
            document.getElementById(player_id).play();
            $(this).parent().addClass('playing');
            $(this).parent().find('.overlay-image').fadeOut(200);
          }
        } else if ($(this).parent().find('.mediaType').attr('data-src') === 'vimeo') {
          player_id = $(this).parent().find('iframe').attr('id');
          var iframe = $(this).parent().find('.mediaType').find('iframe')[0];
          var player = $f(iframe);

          if ($(this).parent().hasClass('playing')) {
            player.api('pause');
            $(this).parent().find('.overlay-image').fadeIn(200);
            $(this).parent().removeClass('playing');
          } else {
            player.api('play');
            $(this).parent().addClass('playing');
            $(this).parent().find('.overlay-image').fadeOut(200);
          }
        } else if ($(this).parent().find('.mediaType').attr('data-src') === 'youtube') {
          player_id = $(this).parent().find('iframe').attr('id');
          if (!$(this).parent().hasClass('playing')) {
            if (!ytPLayer[player_id]) {
              newPlayer = new YT.Player(player_id, {
                events: {
                  'onReady': onReady
                }
              });
            } else {
              ytPLayer[player_id].playVideo();
            }
            $(this).parent().addClass('playing');
            $(this).parent().find('.overlay-image').fadeOut(200);
          } else {
            ytPLayer[player_id].pauseVideo();
            $(this).parent().find('.overlay-image').fadeIn(200);
            $(this).parent().removeClass('playing');
          }
        }
      });


      function onAlways(instance) {
        var x = $('.section-table-form').length;
        for (i = 0; i <= x; i++) {
          $('#section' + i + '-table-form .qtip-div').qtip({
            content: '<img src="' + Drupal.settings.themePath + '/files/qtip/screenshot-sec' + i + '.png" width="300" height="300" alt="Owl" />',
            style: {
              classes: 'qtip-tipsy qtip-shadow',
              width: 700,
            },
            position: {
              my: 'center right',
              at: 'center left',
            }
          });
        }
        $('.sec2-inner img').on('dragstart', function(event) {
          event.preventDefault();
        });

        function getLeftOffset($obj) {
          return parseInt($obj.css('left'), 10);
        }

        function fullDragScreen() {
          var ch = 0;
          var $screen = $('.sec2-content');
          var $jsp = $screen.find('.jspPane');
          var $jstr = $screen.find('.jspTrack');
          var $jsBar = $screen.find('.jspDrag');
          var dragLeftLimit = 0;
          var contentLeftLimit = 0;
          var dragRightLimit = 0 - ($jstr.width() - $jsBar.width());
          var contentRightLimit = 0 - ($('.sec2-inner').outerWidth(true) - parseInt($screen.css('width'), 10));
          var prevLoc = 0;
          $('img').on('dragstart', function(event) {
            event.preventDefault();
          });
          $('.sec2-inner').on('mousedown', function(event) {
            $(this).addClass('dragging-enabled');
            prevLoc = event.screenX;
          });
          $('body').on('mouseup', function() {
            $('.sec2-inner').removeClass('dragging-enabled');
          });
          $('.sec2-inner').on('mousemove', function(event) {
            if ($(this).hasClass('dragging-enabled')) {
              if (prevLoc > event.screenX) {
                ch = prevLoc - event.screenX;
                if (getLeftOffset($jsp) <= contentLeftLimit && getLeftOffset($jsp) >= contentRightLimit) {
                  if ((getLeftOffset($jsp) - ch) <= contentRightLimit) {
                    $jsp.css({
                      'left': contentRightLimit + 'px'
                    });
                  } else {
                    $jsp.css({
                      'left': (getLeftOffset($jsp) - ch) + 'px'
                    });
                  }

                }
              } else if (prevLoc < event.screenX) {
                ch = prevLoc - event.screenX;
                if (getLeftOffset($jsp) <= contentLeftLimit && getLeftOffset($jsp) >= contentRightLimit) {
                  if ((getLeftOffset($jsp) - ch) >= contentLeftLimit) {
                    $jsp.css({
                      'left': contentLeftLimit + 'px'
                    });
                  } else {
                    $jsp.css({
                      'left': (getLeftOffset($jsp) - ch) + 'px'
                    });
                  }
                }
              }
              prevLoc = event.screenX;
              //Update Scrollbar
              percMove = getLeftOffset($jsp) / contentRightLimit;
              tracMovement = percMove * dragRightLimit;
              $jstr.find('.jspDrag').css({
                'left': (0 - tracMovement) + 'px'
              });
            }

          });
        }
        // $('#slider1').nivoSlider();
        if (Drupal.settings.nodeType === 'homepage_fila') {
          $('#section1slider').nivoSlider({
            effect: Drupal.settings.slider1Settings.effect, // Specify sets like: 'fold,fade,sliceDown'
            animSpeed: parseInt(Drupal.settings.slider1Settings.speed, 10), // Slide transition speed
            pauseTime: parseInt(Drupal.settings.slider1Settings.pauseTime, 10), // How long each slide will show
          });

          $("#section1FlexSlider")
            .fitVids()
            .flexslider({
              animation: "fade",
              useCSS: false,
              animationLoop: true,
              slideshowSpeed: parseInt(Drupal.settings.slider1Settings.pauseTime, 10),
              animationSpeed: parseInt(Drupal.settings.slider1Settings.speed, 10),
              smoothHeight: true,
              start: function(slider) {
                var player_id;
                if ($('.sec1 .' + slider[0].classList[0]).find('li').eq(slider.currentSlide).find('.mediaType').attr('data-type') === 'video') {
                  if ($('.sec1 .' + slider[0].classList[0]).find('li').eq(slider.currentSlide).find('.mediaType').attr('data-src') === 'custom') {
                    player_id = $('.sec1 .' + slider[0].classList[0]).find('li').eq(slider.currentSlide).find('video').attr('id');
                    if (slider.currentSlide === 0 || slider.currentSlide === 2 || slider.currentSlide === 4) {
                      document.getElementById(player_id).play();
                    }
                  } else if ($('.sec1 .' + slider[0].classList[0]).find('li').eq(slider.currentSlide).find('.mediaType').attr('data-src') === 'vimeo') {
                    $('.sec1 .' + slider[0].classList[0]).find('li').eq(slider.currentSlide).find('iframe').load(function() {
                      if (slider.currentSlide === 0 || slider.currentSlide === 2 || slider.currentSlide === 4) {
                        var iframe = $('.sec1 .' + slider[0].classList[0]).find('li').eq(slider.currentSlide).find('iframe')[0];
                        var player = $f(iframe);
                        player.api('play');
                      }
                    });
                  } else if ($('.sec1 .' + slider[0].classList[0]).find('li').eq(slider.currentSlide).find('.mediaType').attr('data-src') === 'youtube') {
                    player_id = $('.sec1 .' + slider[0].classList[0]).find('li').eq(slider.currentSlide).find('iframe').attr('id');
                    $('.sec1 .' + slider[0].classList[0]).find('li').eq(slider.currentSlide).find('iframe').load(function() {
                      if (slider.currentSlide === 0 || slider.currentSlide === 2 || slider.currentSlide === 4) {
                        newPlayer = new YT.Player(player_id, {
                          events: {
                            'onReady': onReady
                          }
                        });
                      }
                    });
                  }
                }
                $('.flex-caption').each(function() {
                  $(this).css({
                    'top': ($(this).parents('li').height() - $(this).height()) / 2 + 'px'
                  });
                });
              },
              before: function(slider) {
                var player_id;
                if ($('.sec1 .' + slider[0].classList[0]).find('li').eq(slider.currentSlide).find('.mediaType').attr('data-type') === 'video') {
                  if ($('.sec1 .' + slider[0].classList[0]).find('li').eq(slider.currentSlide).find('.mediaType').attr('data-src') === 'custom') {
                    player_id = $('.sec1 .' + slider[0].classList[0]).find('li').eq(slider.currentSlide).find('video').attr('id');
                    if (slider.currentSlide === 0 || slider.currentSlide === 2 || slider.currentSlide === 4) {
                      document.getElementById(player_id).load();
                    }
                  } else if ($('.sec1 .' + slider[0].classList[0]).find('li').eq(slider.currentSlide).find('.mediaType').attr('data-src') === 'vimeo') {
                    if (slider.currentSlide === 0 || slider.currentSlide === 2 || slider.currentSlide === 4) {
                      var iframe = $('.sec1 .' + slider[0].classList[0]).find('li').eq(slider.currentSlide).find('iframe')[0];
                      var player = $f(iframe);
                      player.api('pause');
                      player.api('seekTo', 0);
                    }
                  } else if ($('.sec1 .' + slider[0].classList[0]).find('li').eq(slider.currentSlide).find('.mediaType').attr('data-src') === 'youtube') {
                    player_id = $('.sec1 .' + slider[0].classList[0]).find('li').eq(slider.currentSlide).find('iframe').attr('id');
                    if (slider.currentSlide === 0 || slider.currentSlide === 2 || slider.currentSlide === 4) {
                      if (!ytPLayer[player_id]) { // Corner Case
                        var checkReady = setInterval(function() {
                            if (ytPLayer[player_id] !== undefined) {
                              ytPLayer[player_id].stopVideo();
                              ytPLayer[player_id].seekTo(0, true);
                              clearInterval(checkReady);
                            }
                          },
                          30);
                      } else {
                        ytPLayer[player_id].stopVideo();
                        ytPLayer[player_id].seekTo(0, true);
                      }
                    }
                  }
                }
              },
              after: function(slider) {
                var player_id;
                if ($('.sec1 .' + slider[0].classList[0]).find('li').eq(slider.currentSlide).find('.mediaType').attr('data-type') === 'video') {
                  if ($('.sec1 .' + slider[0].classList[0]).find('li').eq(slider.currentSlide).find('.mediaType').attr('data-src') === 'custom') {
                    player_id = $('.sec1 .' + slider[0].classList[0]).find('li').eq(slider.currentSlide).find('video').attr('id');
                    if (slider.currentSlide === 0 || slider.currentSlide === 2 || slider.currentSlide === 4) {
                      document.getElementById(player_id).play();
                    }
                  } else if ($('.sec1 .' + slider[0].classList[0]).find('li').eq(slider.currentSlide).find('.mediaType').attr('data-src') === 'vimeo') {
                    if (slider.currentSlide === 0 || slider.currentSlide === 2 || slider.currentSlide === 4) {
                      var iframe = $('.sec1 .' + slider[0].classList[0]).find('li').eq(slider.currentSlide).find('iframe')[0];
                      var player = $f(iframe);
                      player.api('play');
                    }
                  } else if ($('.sec1 .' + slider[0].classList[0]).find('li').eq(slider.currentSlide).find('.mediaType').attr('data-src') === 'youtube') {
                    player_id = $('.sec1 .' + slider[0].classList[0]).find('li').eq(slider.currentSlide).find('iframe').attr('id');
                    if (slider.currentSlide === 0 || slider.currentSlide === 2 || slider.currentSlide === 4) {
                      if (!ytPLayer[player_id]) {
                        newPlayer = new YT.Player(player_id, {
                          events: {
                            'onReady': onReady
                          }
                        });
                      } else {
                        ytPLayer[player_id].playVideo();
                      }
                    }
                  }
                }
              }

            });


          $('#sec3slider').fitVids()
            .flexslider({
              animation: "fade",
              slideshow: false,
              animationLoop: false,
              slideshowSpeed: 3000,
              animationSpeed: 700,
              smoothHeight: true
            });
        }
        var width = 0,
          height = 0;
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || $(window).width() <= 960) {
          var left_pos = 0;
          var count = $('.sec2-inner').find('.sec-item').length;
          $('.sec2-inner').css({
            'width': (count * $(window).width()) + 'px'
          });
          $('.sec2-inner').find('.sec-item').each(function() {
            $(this).css({
              'width': $(window).width() + 'px'
            });
            $(this).find('.image-container').css({
              'max-width': '80%',
              'margin': '0 auto',
              'text-align': 'center'
            });
            $(this).find('.detail-container').css({
              'max-width': '80%',
              'margin': '0 auto'
            });
            $(this).find('.image-asset').css({
              'width': '100%'
            });
            var heightFactor = $(this).find('.image-asset').width();
            // alert(heightFactor);
            if ($(this).find('.mediaType').attr('data-type') === 'video') {
              $(this).find('.image-asset').css({
                'height': (392 / 588) * heightFactor + 'px'
              });
            }
          });
          prevX = 0;
          $('.sec2 .image-asset').bind('touchstart', function(e) {
            e.preventDefault();
            var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
            if (touch.pageX)
              prevX = touch.pageX;
          });
          $('.sec2 .image-asset').bind('touchend', function(e) {
            e.preventDefault();
            var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
            if (touch.pageX) {
              if (prevX < touch.pageX) {
                $('.sec2').find('.info-wrapper').fadeOut(500);
                if (left_pos < 0) {
                  left_pos += $(window).width();
                  $('.sec2-inner').animate({
                    'left': left_pos + 'px'
                  }, 200);
                }
              } else if (prevX > touch.pageX) {
                $('.sec2').find('.info-wrapper').fadeOut(500);
                if (left_pos !== (0 - ((count - 1) * $(window).width()))) {
                  left_pos -= $(window).width();
                  $('.sec2-inner').animate({
                    'left': left_pos + 'px'
                  }, 200);
                }
              }
            }
          });
          left_pos -= $(window).width();
          $('.sec2-inner').animate({
            'left': left_pos + 'px'
          }, 200);
          prevsX = 0;
          $('.play-pause-btn').bind('touchstart', function(e) {
            e.preventDefault();
            var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
            if (touch.pageX)
              prevsX = touch.pageX;
          });
          $('.play-pause-btn').bind('touchend', function(e) {
            e.preventDefault();
            var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
            if (touch.pageX) {
              if (prevsX === touch.pageX) {
                $(this).trigger('click');
              }
            }
          });
          prev3X = 0;
          $('#sec3slider').bind('touchstart', function(e) {
            e.preventDefault();
            var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
            if (touch.pageX)
              prev3X = touch.pageX;
          });
          $('#sec3slider').bind('touchend', function(e) {
            e.preventDefault();
            var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
            if (touch.pageX) {
              if (prev3X > touch.pageX) {
                $(".sec3 a.nivo-prevNav").trigger('click');
              } else if (prev3X < touch.pageX) {
                $(".sec3 a.nivo-nextNav").trigger('click');
              }
            }
          });
          if ($(window).width() < 680) {
            $('.sec1 .slides li').each(function() {
              var lef = 0 - ($(this).children().width() - $(this).width()) / 2;
              $(this).find('img').css({
                'left': lef + 'px'
              });
              $(this).find('iframe').css({
                'left': lef + 'px'
              });
            });
          }
        } else {
          $('.flex-caption > div').hover(function() {
            $(this).parents('li').find('img').addClass('zoomed');
          }, function() {
            $(this).parents('li').find('img').removeClass('zoomed');
          });
          $('.sec2-inner').find('.sec-item').each(function() {
            width += parseInt($(this).css('margin-right'), 10) + $(this).find('.image-asset').width();
            var imgWidth = $(this).find('.image-container .image-asset').width();
            $(this).css({
              'width': imgWidth + 'px'
            });
          });
          $('.sec2-inner').css({
            'width': width + 'px'
          });
          $('.sec2-content').jScrollPane();
          fullDragScreen();
        }
        //
        $('.sec2-content').find('.overlay-image').addClass('show-images');
        $('.sec2-inner').find('.sec-item').each(function() {
          width += $(this).outerWidth(true);
          var imgWidth = $(this).find('.image-container .image-asset').width();
          $(this).find('.image-container').css({
            'width': imgWidth + 'px'
          });
          $(this).find('.play-pause-btn').css({
            height: $(this).find('.image-container').find('.image-asset').height()
          });
          $(this).find('.overlay-image').css({
            height: $(this).find('.image-container').find('.image-asset').height()
          });
          if (height < $(this).find('.image-container').find('.image-asset').height()) {
            height = $(this).find('.image-container').find('.image-asset').height();
          }
        });
        $('.sec-item .image-container').css({
          'height': height + 'px'
        }).addClass('images-processed');

        var timer = setInterval(function() {
          if (parseInt($('.sec2').find('.jspPane').css('left'), 10) < 0) {
            $('.sec2').find('.info-wrapper').fadeOut(500);
          }
        }, 100);
        //for sec3
        $('#sec3slider .flex-control-paging').css({
          'top': $('#sec3slider').find('.slides li img').height() + 'px'
        });
        //for sec 4z
        $('.section-half').find('.explore').click(function() {
          $(this).parents('.section-half').find('.section-items').fadeToggle(250);
        });
        $('.section-half').find('.section-items').find('.close-button').click(function() {
          $(this).parents('.section-half').find('.section-items').fadeOut(250);
        });
        $('.CORScontentIsLoading').removeClass('CORScontentIsLoading');
        $('.CORScontentLoaderDiv').remove();
      }
      // bind with .on()
      imgLoad.on('always', onAlways);
      $(window).scroll(function() {
        if (window.scrollY + window.innerHeight >= $('.sec2').offset().top + ($('.sec2').height() / 2) + $('.sec2 .info-wrapper').height()) {
          $('.sec2 .info-wrapper').delay(6000).fadeOut(300);
        }
      });
    }
  };
})(jQuery);
