<?php

/**
 * @file field.tpl.php
 * Default template implementation to display the value of a field.
 *
 * This file is not used and is here as a starting point for customization only.
 * @see theme_field()
 *
 * Available variables:
 * - $items: An array of field values. Use render() to output them.
 * - $label: The item label.
 * - $label_hidden: Whether the label display is set to 'hidden'.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - field: The current template type, i.e., "theming hook".
 *   - field-name-[field_name]: The current field name. For example, if the
 *     field name is "field_description" it would result in
 *     "field-name-field-description".
 *   - field-type-[field_type]: The current field type. For example, if the
 *     field type is "text" it would result in "field-type-text".
 *   - field-label-[label_display]: The current label position. For example, if
 *     the label position is "above" it would result in "field-label-above".
 *
 * Other variables:
 * - $element['#object']: The entity to which the field is attached.
 * - $element['#view_mode']: View mode, e.g. 'full', 'teaser'...
 * - $element['#field_name']: The field name.
 * - $element['#field_type']: The field type.
 * - $element['#field_language']: The field language.
 * - $element['#field_translatable']: Whether the field is translatable or not.
 * - $element['#label_display']: Position of label display, inline, above, or
 *   hidden.
 * - $field_name_css: The css-compatible field name.
 * - $field_type_css: The css-compatible field type.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess_field()
 * @see theme_field()
 *
 * @ingroup themeable
 */
?>
    <?php foreach ($items as $delta => $item): ?>
      <?php
        $file = $item['#file'];
      ?>
        <?php
        if($file->type === 'video'){
          if($file->filemime === 'video/vimeo'){
            $vid = explode('/' , $file->uri);
           print '
           <div class="mediaType" data-type="video" data-src="vimeo">
           <iframe id="player'. $delta .'" src="http://player.vimeo.com/video/' . $vid[count($vid) - 1] . '?api=1&player_id=player'. $delta .'" width="1366" height="645" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
           </div>
           ';
          }
          else if( $file->filemime === 'video/youtube'){
            $vid = explode('/' , $file->uri);
            print '<div class="mediaType" data-type="video" data-src="youtube">
            <iframe width="1366" height="645" id="player' . $delta . '" src="https://www.youtube.com/embed/' . $vid[count($vid) - 1] .'?enablejsapi=1&playerapiid=ytplayer&version=3&autoplay=0&showinfo=0&controls=0&modestbranding=1&iv_load_policy=3" frameborder="0" allowfullscreen></iframe>
            </div>';
          }
          else {
            $file_url = file_create_url($file->uri);
            print '
            <div class="mediaType" data-type="video" data-src="custom">
            <video width="1366" height="645" style="width: 1366px; height: 645px;" id="player'. $delta  .'" >
              <source src="'. $file_url .'" type="'. $file->filemime .'" width="1366" height="645">
              </source>
              Your browser does not support the video tag.
            </video> </div>';
          }

        }
        else if($file->type === 'image'){
          //Cropping the image at run time
          image_style_create_derivative(image_style_load('section_1_image'), $file->uri , image_style_url('section_1_image', $file->uri));
          $file_url = file_create_url($file->uri);
          $alt = count($file->field_file_image_alt_text)  === 0 ? '' : $file->field_file_image_alt_text['und'][0]['value'];
          $title = count($file->field_file_image_title_text) === 0 ? '' : $file->field_file_image_title_text['und'][0]['value'] ;
          // Setting up the array for new sourced Image
          $croppedImage = array(
            'style_name' => 'section_1_image',
            'path' => $file->uri,
            'width' => $file->metadata['width'],
            'height' => $file->metadata['height'],
            'alt' => $alt,
            'title' => $title
            );
            // Rendering the cropped Image
           print(theme('image_style',$croppedImage));
        }
      ?>
    <?php endforeach; ?>
